package org.jbox2d.testbed.tests;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javafx.scene.paint.Color;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.particle.ParticleGroup;
import org.jbox2d.particle.ParticleGroupDef;
import org.jbox2d.particle.ParticleType;
import org.jbox2d.testbed.framework.ContactPoint;
import org.jbox2d.testbed.framework.TestbedSettings;
import org.jbox2d.testbed.framework.TestbedTest;

/**
 * @author Christopher Towner
 */
public class Hearts extends TestbedTest {

    private static final Vec2 GRAVITY = new Vec2(0, -30);
    private static final int HEART_MAX = 5;
    private static final int HEART_DELAY = 3;
    private static final float PARTICLE_DELAY = 2.5f;

    private List<Body> hearts;
    private float time;
    private float heartInterval;

    private Body cloud;
    private int score;

    @Override
    public boolean isSaveLoadEnabled() {
        return false;
    }

    @Override
    public void initTest(boolean deserialized) {
        hearts = new LinkedList<>();
        heartInterval = 0;
        time = 0f;
        score = 0;

        getWorld().setGravity(GRAVITY);

        Body ground = getWorld().createBody(new BodyDef());

        EdgeShape edge = new EdgeShape();
        edge.set(new Vec2(-30.0f, 0.0f), new Vec2(30.0f, 0.0f));
        ground.createFixture(edge, 0.0f);

        edge.set(new Vec2(-30.0f, 0.0f), new Vec2(-30.0f, 5.0f));
        ground.createFixture(edge, 0.0f);

        edge.set(new Vec2(30.0f, 0.0f), new Vec2(30.0f, 5.0f));
        ground.createFixture(edge, 0.0f);

        createCloud();

        m_world.setParticleRadius(0.25f);
        m_world.setParticleDamping(1);
    }

    private void createHeart() {
        PolygonShape poly = new PolygonShape();
        poly.setAsBox(0.5f, 0.5f);

        CircleShape circle1 = new CircleShape();
        circle1.m_radius = 0.5f;
        circle1.m_p.set(0f, 0.5f);

        CircleShape circle2 = new CircleShape();
        circle2.m_radius = 0.5f;
        circle2.m_p.set(0.5f, 0f);

        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;

        float x = MathUtils.randomFloat(-20.0f, 20.0f);
        bd.position.set(x, 30.0f);
        bd.angularVelocity = MathUtils.randomFloat(-2f, 2f);;
        float r = MathUtils.randomFloat(0.0f, MathUtils.TWOPI);
        bd.setAngle(r);
        Body body = getWorld().createBody(bd);

        FixtureDef fd = new FixtureDef();
        fd.density = 1.0f;
        fd.friction = 0.3f;
        fd.stroke = Color.RED;
        fd.fill = Color.RED;

        fd.shape = poly;
        body.createFixture(fd);
        fd.shape = circle1;
        body.createFixture(fd);
        fd.shape = circle2;
        body.createFixture(fd);

        hearts.add(body);
    }

    public void destroyHeart(Body body) {
        Vec2 pos = body.getPosition();

        CircleShape shape = new CircleShape();
        shape.m_p.set(pos.x, pos.y);
        shape.m_radius = 2.5f;
        ParticleGroupDef pd = new ParticleGroupDef();
        pd.flags = ParticleType.b2_waterParticle;
        pd.shape = shape;
        pd.userData = time + PARTICLE_DELAY;

        m_world.createParticleGroup(pd);

        hearts.remove(body);
        m_world.destroyBody(body);
        score++;
    }

    private void createCloud() {
        PolygonShape poly = new PolygonShape();
        Vec2[] vertices = new Vec2[]{
            new Vec2(0, 0),
            new Vec2(0, 5),
            new Vec2(30, 5),
            new Vec2(30, 0)
        };
        poly.set(vertices, 4);

        CircleShape circle1 = new CircleShape();
        circle1.m_radius = 2.5f;
        circle1.m_p.set(0f, 2.5f);

        CircleShape circle2 = new CircleShape();
        circle2.m_radius = 2.5f;
        circle2.m_p.set(30f, 2.5f);

        CircleShape circle3 = new CircleShape();
        circle3.m_radius = 2.5f;
        circle3.m_p.set(2.5f, 5f);

        CircleShape circle4 = new CircleShape();
        circle4.m_radius = 5f;
        circle4.m_p.set(10f, 5f);

        CircleShape circle5 = new CircleShape();
        circle5.m_radius = 5f;
        circle5.m_p.set(25f, 5f);

        CircleShape circle6 = new CircleShape();
        circle6.m_radius = 7.5f;
        circle6.m_p.set(17.5f, 7.5f);

        BodyDef bd = new BodyDef();
        bd.type = BodyType.STATIC;

        //float x = MathUtils.randomFloat(-20.0f, 20.0f);
        float x = 40;
        bd.position.set(x, 20.0f);

        cloud = getWorld().createBody(bd);

        FixtureDef fd = new FixtureDef();
        fd.density = 1.0f;
        fd.friction = 0.3f;
        fd.stroke = Color.WHITE;
        fd.fill = Color.WHITE;

        fd.shape = poly;
        cloud.createFixture(fd);
        fd.shape = circle1;
        cloud.createFixture(fd);
        fd.shape = circle2;
        cloud.createFixture(fd);
        fd.shape = circle3;
        cloud.createFixture(fd);
        fd.shape = circle4;
        cloud.createFixture(fd);
        fd.shape = circle5;
        cloud.createFixture(fd);
        fd.shape = circle6;
        cloud.createFixture(fd);
    }

    @Override
    public void keyPressed(char argKeyChar, int argKeyCode) {
        switch (argKeyChar) {
            default:
                break;
        }
    }

    @Override
    public void step(TestbedSettings settings) {
        super.step(settings);
        float hz = settings.getSetting(TestbedSettings.Hz).value;
        if (hz > 0) {
            time += 1.0f / hz;
        }

        //check if heart should be spawned
        if (time > heartInterval && hearts.size() < HEART_MAX) {
            createHeart();
            heartInterval += HEART_DELAY;
        }

        //clear old particles from map
        ParticleGroup[] particles = m_world.getParticleGroupBuffer();
        if (particles != null) {
            for (ParticleGroup pg : particles) {
                Object o = pg.getUserData();
                if (o != null) {
                    float expiration = (float) o;
                    if (time > expiration) {
                        m_world.destroyParticlesInGroup(pg);
                    }
                } else {
                    m_world.destroyParticlesInGroup(pg);
                }
            }
        }

        // Traverse the contact results. Destroy bodies that are touching cloud.
        for (int i = 0; i < getPointCount(); ++i) {
            ContactPoint point = points[i];

            Body body1 = point.fixtureA.getBody();
            Body body2 = point.fixtureB.getBody();

            if (body1 == cloud) {
                destroyHeart(body2);
            } else if (body2 == cloud) {
                destroyHeart(body1);
            }
        }

        addTextLine("time = " + time);
        addTextLine("score = " + score);
    }

    @Override
    public String getTestName() {
        return "iHeartMedia";
    }
}
